.. title: OFC 2018 Labautomation Hackathon
.. slug: ofc-2018-labautomation-hackathon
.. date: 2018-03-13 23:34:17 UTC+01:00
.. tags: labautomation,ofc,hackathon
.. author: Jochen Schröder
.. category: 
.. link: 
.. description: Summary from the OFC 2018 Labautomation Hackathon
.. type: text

OFC 2018 Labautomation Hackathon wrap-up
========================================

We held another labautomation hackathon the at the Optical Fiber Communication (OFC) Conference in San Diego last Sunday (11/3/2018). This again was another great event, with about 50-70 participants.

After the events at OFC 2017 and ECOC 2017 we wanted to try something a bit different to get even more interaction between the audience and the speakers/demonstrators. After a brief introduction from Binbin Guan from Acacia, Nicolas Fontaine from Nokia Bell Labs and myself (Jochen Schröder from Chalmers University of Technology), demonstrators/speakers moved to individual tables where they showed off their technology/work. Every 20 minutes participants could move to a new table to learn something new.

We thought this worked really well and there was a lot of discussions at the different tables. It also allowed everyone to get quite a bit more into the details of the different demonstrations.

We had the following great topics and speakers:

* Getting started -- Nick Fontaine, Nokia Bell Labs
* Basic DSP in Python -- Ruben Luis, NICT, Rasmus Thomas Jones DTU Fotonik
* Nazca design for Photonic IC layout -- Ronald Broeke, Bright Photonics
* Python debugging and unit testing -- Marco Rizzi, Facebook
* Getting Started with Keysight Command Expert (KCE) and Python -- John Dorighi, Keysight Technologies
* Efficient modeling and optimization for silicon photonic components -- Adam Reid, Jackson Klein, Lumerical Inc.
* Model fitting with python and circuit simulation -- Pieter Dumon, Luceda Photonics
* DSP using open source QAMpy -- Jochen Schröder, Mikael Mazur, Chalmers University of Technology

Ardavan Oskooi from Simpetus had to unfortunately cancel is talk about MEEP at the last minute, due to illness.

The energy in the room was really great, largely thanks to the awesome job our speakers did. The beer and pizza might have helped a bit as well, thanks again to the OSA for sponsoring this event.

We are starting to upload some of the content that was used to our `gitlab.com <https://gitlab.com>`_ repository which you can find `here <https://gitlab.com/python4photonics/LabAutomationHackathons>`_. We are hoping that this will proof to be a great resource for others who could not make it to the hackathons to see a bit about what was going on.

We are planning to run this event again next year and are also currently evaluating if we can run a similar event again at ECOC in Rome. We might also try to run similar events had other conferences so keep a lookout on what’s happening. 
