.. title: How to get started and where to find help
.. date: 2017-09-11 20:00:00 UTC-03:00
.. tags: 
.. link: 
.. slug: gettingstarted
.. description: How to start learning python for use in your photonics research and where to ask for help when you are stuck.


One, if not *the* best thing about Python is the awesome community. While it is used in many large scale professional programs, Python always had an aim of attracting and inspire beginners to start programming and enjoy it. This clearly reflects in how the community treats newcomers and beginners. People are generally very friendly and patient and are happy to answer even very basic questions.

There are also lots of tutorials and beginner guides out there you can have a look at the Python `Getting Started <https://www.python.org/about/gettingstarted/>`_ page. The Python wiki also contains a large `list of tutorials and resources <https://wiki.python.org/moin/BeginnersGuide/Programmers>`_.

Numerical and Scientific Python
===============================

While the choices for tutorials and guides on numerical and scientific python are more limited, this area has exploded in recent years and you can find lots of information through simple google searches.

Python is very modular, i.e. you include functionality by “importing” modules (similar to libraries in C or C++). Therefore much of the numerical and scientific functionality is not part of the core of python, but instead in separate modules. The four modules that are at the centre of the Python scientific stack are:

`numpy <https://numpy.org>`_
    The numpy package is the fundamental package scientific work in Python. It contains the array object, which underlies most scientific computations in python, broadcasting functions and tools, some useful tools such as FFTs, basic linear algebra etc..

`scipy <https://scipy.org>`_
    Scipy is the broad collection of additional tools, ranging from linear algebra routines (including BLAS routines) to signal and image processing functions.

`matplotlib <https://matplotlib.org>`_ 
    Matplotlib is the main plotting tool in scientific Python. It is useful both for interactive plots when doing, e.g. analysis in the interpreter, as well as producing beautiful publication ready graphs.

`IPython <https://ipython.org>`_ now part of `jupyter <https://jupyter.org>`_
    Ipython makes working interactively with python easy. In an interactive session either in a browser or as a console application, ipython provides a comfortable interface with command history, help, plotting etc., which makes exploring and working with data easy.


Fortunately, the anaconda distribution already includes these packages, so you do not have to worry about how to install them.

To get started programming with the scientific stack in python, it is good to begin with the `numpy quickstart tutorial <https://docs.scipy.org/doc/numpy-dev/user/quickstart.html>`_ and the `scipy getting started guide <https://scipy.org/getting-started.html>`_. Another great course can be found `here <https://www.python-course.eu/numerical_programming.php>`_. 

.. Note:: If you are already familiar with Matlab, we recommend to have a look at the `numpy for matlab users guide <https://docs.scipy.org/doc/numpy-dev/user/numpy-for-matlab-users.html>`_  which explains many of the similarities and differences between numpy and matlab, and points out some of the pitfalls you might encounter.


Books
----- 

In the last years the number of books on scientific python has virtually exploded and a quick `search <https://www.amazon.co.uk/s/ref=nb_sb_noss?url=search-alias=aps&field-keywords=scientific+python&rh=i:aps,k:scientific+python>`_ on Amazon returns a large number of results. We clearly can not review all of these. Two books that we found useful are:

* “Numerical Methods in Engineering with Python 3“ by Jaan Kiusalaas (`amazon link <https://www.amazon.com/Numerical-Methods-Engineering-Python-3/dp/1107033853>`_) and 

* "A Primer on Scientific Programming with Python (Texts in Computational Science and Engineering)” by Hans Petter Langtangen (`book on amazon <https://www.amazon.co.uk/Scientific-Programming-Computational-Science-Engineering-ebook/dp/B01JEJNWGG/ref=sr_1_2?ie=UTF8&qid=1505252397&sr=8-2&keywords=scientific+python>`_)

Generally it is a good idea to look for the latest edition of books and do not buy books more than 2 years old, as the field of scientific python is still moving very rapidly. 


Where to ask for help when you are stuck
========================================

There are many places where to ask for help, which one is best often depends on your problem. `Stackoverflow <https://stackoverflow.com>`_ is a good place to ask user questions. Remember to tag your question appropriately with e.g. `python`, `numpy`, `scipy` or `matplotlib`. For more specific questions on `numpy` or `scipy`, there are also mailing lists which can be found on this `page <https://scipy.org/scipylib/mailing-lists.html>`_. For other projects it is often a good idea to check on the website what their preferred way of contact is.


Starting to program
===================

The best and easiest way to start with is using `jupyter <https://jupyter.org>`_ which is a tool build for letting us easily code, study and analyse data and debug our programs. It initially developed out of the `ipython notebook` specifically for Python, but is now capable of running other languages as well. To start working with jupyter type `jupyter notebook` into a commandline prompt. After that your browser should pop up with a website with a list of files (usually the directory where you typed the above command). You can start a new notebook using the button on the top right where depending if you installed Python2, Python3 or both you should see several options. Choose the `Python 3` option to start a Python 3 notebook.

The notebook is an interactive programming environment in the browser. You can also write documentation along with your code using `Markdown` text, which can be great for documenting your work. Giving a full introduction into the notebook is well beyond this text, below you find some tutorials how to work with python and notebooks. The great thing is these are notebook files themselves, so you can view them either in the browser, or download them and use them as a starting point for getting to know python. An important thing to remember is that `<Shift-Return>` executes the current cell and advances to the next one.

* `Programming Python Style <http://nbviewer.jupyter.org/github/jrjohansson/scientific-python-lectures/blob/master/Lecture-1-Introduction-to-Python-Programming.ipynb>`_
* `Using numpy <http://nbviewer.jupyter.org/github/jrjohansson/scientific-python-lectures/blob/master/Lecture-2-Numpy.ipynb>`_ As mentioned above numpy is the most important package for numerical and scientific work in python.
* `Plotting data using matplotlib <http://nbviewer.jupyter.org/github/jrjohansson/scientific-python-lectures/blob/master/Lecture-4-Matplotlib.ipynb>`_ 
* `Using scientific functions in pyhton <http://nbviewer.jupyter.org/github/jrjohansson/scientific-python-lectures/blob/master/Lecture-3-Scipy.ipynb>`_


Some more advanced topics
=========================

I have a lot of existing Labview/Matlab code, do I need to redo everything
    Fortunately there are packages that allow you to access labview and matlab code. So you don’t need to convert everything at once.

I have lots of C/Fortran code
   One of the great things about python is how easy it is to integrate with C and Fortran code. The easiest way to use C-libraries is probably `ctypes <https://docs.python.org/2/library/ctypes.html>`_. However it has some performance implications, so if you need to call a C-function in a tight loop you will see a slow down. Also sometimes you only want to integrate small bits of C-code, or do some more manipulation. `Cython <http:/cython.org>`_ is essentially a superset of python that can be compiled to binary. It allows easy integration of python code with C-functions or libraries and is extremely powerful. For Fortran code you should look at `f2py <https://docs.scipy.org/doc/numpy-dev/f2py/>`_ which allows easy linking python to Fortran code (note that you can also use Cython for this). A great way of experimenting with `f2py` is to use `fortran magic <https://pypi.python.org/pypi/fortran-magic>`_ which allows you to enter Fortran code directly into the notebook.

What about instrument control?
   The main package for accessing (GPIB, USB …) instruments are the python bindings to the VISA library `pyvisa <http://pyvisa.readthedocs.io/en/stable/>`_. Some other interesting packages that are more high-level are `python-ivi <https://github.com/python-ivi/python-ivi>`_ which contains a lot of drivers for oscilloscopes, signal generators and more. It can also use  `pyvisa` under the hood. You will find quite a few other higher-level projects build on top of pyvisa.

What about building a GUI?
   It depends a lot what type of GUI you want. You can make interactive notebooks, by integrating widgets into the notebook using the `ipywidgets <https://github.com/jupyter-widgets/ipywidgets>`_. Another option for browser based interfaces is `bokeh <http://bokeh.pydata.org/en/latest/>`_. For native GUIs we recommend to use `PyQT <https://riverbankcomputing.com/software/pyqt/intro>`_ with two great tutorials for the older `qt4 <http://zetcode.com/gui/pyqt4/>`_ and the newer `qt5 <http://zetcode.com/gui/pyqt5/>`_. For integrating plotting with you `pyqt` application the best is to use `pyqtgraph <http://www.pyqtgraph.org/documentation/introduction.html#examples>`_.


