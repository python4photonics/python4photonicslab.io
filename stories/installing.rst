.. title: Installing Python and Jupyter Notebook 
.. date: 2017-08-28 20:00:00 UTC-03:00
.. tags: 
.. link: 
.. slug: installing
.. description: How to install Python for use in the lab


Python is user friendly scripting language.  Separately installed packages provide the tools to do science and lab automation.  Finding the correct packages and installing these packages are the biggest stumbling blocks for first time users. Fortunately, there are a few prepackaged python distributions with most of the neccessary scientific libraries including Scipy, Numpy, Matplotlib and Jupyter notebook.


Anaconda Python distribution
------------------------------

If you are not familiar with Python the easiest way to install it is probably the anaconda distribution which can be found `here <https://www.continuum.io/downloads>`_. This works for all operating systems (Windows/Mac OS X/Linux), and includes most of the useful packages/libraries that we need to use.
             
.. Note:: Python has two stable (and slightly incompatible) versions Python 2.7.x and Python 3.6.x. Python 2.7.x version is somewhat more stable,
   but does not support some cool features. **We recommended** Python 3.6.x which is the future of Python. That said, even if you install Python 2.7.x , it is straightforward to build the 3.6.x version after installation without worrying about version conflicts.
 
To install Annaconda is easy, simply download and when installing you can keep clicking ‘continue’ or ‘next’ to finish the installation. More details can be found in the `anaconda documentation <https://conda.io/docs/user-guide/tasks/manage-python.html>`_.
After installation, you can try to open a command prompt (or command terminal in OS X), then type ``pip list``. This will list all of the installed python packages for you.






