.. title: Links
.. slug: links
.. date: 2018-03-25 17:04:23 UTC+02:00
.. tags: links,python,photonics
.. category: 
.. link: 
.. description: Links to photonics software in Python
.. type: text


Python4Photonics Repository
===========================

Some of the notebooks from the Labautomation Hackathons can be found in our `gitlab repository <https://gitlab.com/python4photonics/LabAutomationHackathons>`_.



Photonics projects using Python
===============================

Below is a list of photonics projects written in Python. If you like to have your project included please email us at `Python4Photonics developers <mailto:jochen.schroeder@chalmers.se>`_.


Digital Signal Processing/Equalisation
--------------------------------------

`QAMpy <https://github.com/ChalmersPhotonicsLab/QAMpy>`_
    QAMpy is an open source DSP chain for simulation and equalisation of signals from optical communication transmissions. It is designed with performance in mind and runs a CMA equaliser in under 100 ms on a regular laptop.

Integrated Photonics Design
---------------------------

`Nazca <https://www.nazca-design.org>`_
    Nazca is an open-source PIC design tool based on Python-3. Nazca can also run in Jupyter notebooks, using matplotlib to visualize mask layouts. Nazca also works well together with the high-end open-source Klayout mask viewer/editor, which is extremely popular in the photonics community. Nazca makes design simpler and more fun by adding many features critical for the designer to deliver high-quality designs efficiently in a Python environment. This ranges from embedding hierarchical design in the script syntax to visualisation schemes in mask layout. 

EM simulations
---------------------

`CAMFR <https://github.com/demisjohn/CAMFR>`_
    CAMFR (CAvity Modelling FRamework) is a Python module providing a fast, flexible, full-vectorial Maxwell solver for electromagnetics simulations. Its main focus is on applications in the field of nanophotonics, like wavelength-scale microstructures (like photonic crystal devices, optical waveguides), lasers (like vertical-cavity surface-emitting lasers), light-emitting diodes (like resonant-cavity LEDs). It is based on a combination of eigenmode expansion (EME) and advanced boundary conditions like perfectly matched layers (PML).

`EMpy <https://github.com/lbolla/EMpy>`_
    EMpy - Electromagnetic Python is a suite of algorithms widely known and used in electromagnetic problems and optics: the transfer matrix algorithm, the rigorous coupled wave analysis algorithm and more.


