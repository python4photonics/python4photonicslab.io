.. title: About Python for Photonics
.. slug: about
.. date: 2017-08-28 20:00:00 UTC-03:00
.. template: page.tmpl
.. tags: 
.. link: 
.. description: 

We are a several optics and photonics researchers with together more than 10 years of laboratory and labautomation experience. We have found that using Python as our programming language offers us a cheaper, much more fun alternative, that offers significantly stronger interoperability and functionality than comparable commercial software. We would like to share our interest, passion and knowledge for Python to allow photonics students and researchers to investigate other options to the commercial packages which still dominate many optics research labs. We want this website and the accompanying project to serve as a resource for other researchers and students in photonics on how to get started using Python. We will also post information about the OFC and ECOC LabAutomation Hackathons. 

