.. title: LabAutomation Hackathon 
.. date: 2017-08-28 20:00:00 UTC-03:00
.. tags: 
.. link: 
.. description: Information about the LabAutomation hackathons at OFC and ECOC

Lab work is most efficient when data can be acquired in an automated way without introducing human error, and allowing researchers to concentrate on the fun part of experimental work. Open Source software and easy to learn languages such as `Python <https://www.python.org>`__ are a great and fun way for achieving your lab automation needs and offer as much or more features/interoperability for lab automation as alternative commercial software. 

At `Optical Fiber Communications conference (OFC) <http://www.ofcconference.org>`__ 2016 in LA we organised a workshop to show the fun of using Python and other Open Source software  for laboratory automation. Despite the rather *hasty* organisation and our relative inexperience, this workshop was a great success. We are therefore planning to hold similar workshops at future OFCs and at the `European Conference on Optical Communications (ECOC) <http://ecoc2017.org>`__.

ECOC 2017
---------

The next LabAutomation Hackathon will be held at ECOC 2017 in Gothenburg Sweden

Date and Time
    Sunday 17th of September, 19:30 - 22:00

Location
    Svenska Mässan Room G1

Organisers
    * Jochen Schröder, Chalmers University of Technology
    * Nicolas Fontaine, Nokia Bell Labs
    * BinBin Guan, Acacia Communications

Speakers
  * Ronald Broeke, *Bright Photonics* - “Open source IC design with Nazca”
  * Pieter Dumon, *Luceda Photonics* - “Regression testing of component libraries and designs”
  * Jeff Dralla, Rolf Madsen, *Keysight* - “Python Automation for Test & Measurement with TAP”
  * Andre Richter, Sergei Migaleev, *VPI Photonics* - Python-based IDE for integrated photonic waveguides and optical fibers
  * Mikael Mazur, *Chalmers University of Technology* - Starting with Python and controlling an optical switch
   


OFC 2018
---------

We will hold a Hackathon at OFC 2018 in San Diego, see the OFC conference `Website <http://www.ofcconference.org/en-us/home/program-speakers/special-events/>`_ for details.

