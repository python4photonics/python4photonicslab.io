.. title: ECOC 2017 Hackathon Preparation
.. slug: instructionsecoc2017
.. date: 2017-09-15 23:54:53 UTC+02:00
.. tags: ECOC2017
.. category: 
.. link: 
.. description: Instructions for preparing for the ECOC 2017 Hackathon
.. type: text

We are aiming for this workshop to be an interactive session where you can take part in some of the programming exercises, so bring your laptop along.

To make it easier to join the exercises we recommend that you install a Python environment. Please have a look at the :doc:`How to install Python <installing>` section if you want try installing your Python environment. You can also have a look at :doc:`How to start learning Python <gettingstarted>` section for some links to tutorials etc.. If you do not want to (or cannot) install a Python environment on your computer, we hope to provide a way for you to run python inside your browser without installing.

.. Note:: Below we will post instructions from some of the presenters. Please check again before the workshop, because we will add more material as we receive it. We will also try to make some more of the material available after the workshop and will link to it from here and possibly from the main website.

**VPI**

Please find download and installation instructions for VPImodeDesigner 1.0.1 which will be presented by VPIphotonics during the hackathon, in this `pdf-file </VPImodeDesigner-1.0.1-Installation_ECOC.pdf>`_.

For any questions or problems please contact us by email (support@vpiphotonics.com) or visit
the VPIphotonics Booth 334 at ECOC exhibition.


**Bright Photonics/Nazca**

You can find the `Nazca <http://nazca-design.org>`_ package, documentation and tutorial files used for the workshop at this `location </nazca_ecocdemo.zip>`_. To install the package unzip the nazca package zip file contained in the download and go to the directory using a cmd window. You can install the package with `python setup.py install`. If you get an error about missing `pyqt5` or `pyclipper` you will have to install these using `conda install pyqt5` and `conda install pyclipper` if you used the anaconda distribution or using `pip` otherwise.
